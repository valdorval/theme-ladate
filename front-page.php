<?php
get_header();
get_template_part("template-parts/page", "header");
get_template_part("template-parts/page", "author");
get_template_part("template-parts/page", "book");
get_template_part("template-parts/page", "review");
get_template_part("template-parts/page", "order");
get_footer();
