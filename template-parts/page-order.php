<section class="container commande">
        <header class="m-title has-text-centered">
                <h1 class="title is-2 is-spaced mgb-large">Restez au courant des prochaines sorties.</h1>
        </header>
        <section>
                <div class="column is-half is-offset-one-quarter">
                        <p>Avec l'infolettre de Mark vous restrez au courant de ses toutes nouvelles activités et vous saurez quand il lancera son prochain tôme.</p>
                </div>
                <div class="columns">
                        <div class="column is-half is-offset-one-quarter">

                                <div class="field">
                                        <label class="label">Nom complet</label>
                                        <div class="control">
                                                <input class="input" type="text" placeholder="Nom">
                                        </div>
                                </div>
                                <div class="field">
                                        <label class="label">Courriel</label>
                                        <div class="control has-icons-left has-icons-right">
                                                <input class="input" type="email" placeholder="Votre courriel">
                                        </div>
                                        <div class="field">
                                                <div class="control">
                                                        <label class="checkbox">
                                                                <input type="checkbox">
                                                                J'accepte les termes et conditions
                                                        </label>
                                                </div>
                                        </div>

                                        <div class="field is-grouped">
                                                <div class="control">
                                                        <button class="button is-link">Submit</button>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </section>
</section>