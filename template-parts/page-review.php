<!-- Section  -->
<section class="container crits">
        <header class="m-title">
                <h1 class="title is-2 is-spaced mgb-large">Ce que disent les critiques</h1>
        </header>
        <div class="tile is-ancestor">
                <article class="tile">
                        <div class="tile is-parent">
                                <article class="tile is-child notification is-success">
                                        <p class="subtitle">Cette trilogie est le chef-d'oeuvre de toutes mes soiree a la prison.</p>
                                        <p class="name">Ulrich Massicotte - Gardien de prison</p>
                                </article>
                        </div>
                        <div class="tile is-parent">
                                <article class="tile is-child notification is-primary">
                                        <p class="subtitle">J'aime beaucoup le style de Monsieur Ladate, c'est brun et 1970 a souhait.</p>
                                        <p class="name">Farnande Remington - Retraitée</p>
                                </article>
                        </div>
                        <div class="tile is-parent">
                                <article class="tile is-child notification is-danger">
                                        <p class="subtitle">UN excellent moyen de m'endormir rapidement pour éviter les demandes de mon méri</p>
                                        <p class="name">Natrelle Lactée - Fermière</p>
                                </article>
                        </div>
                        <div class="tile is-parent">
                                <article class="tile is-child notification is-success">
                                        <p class="subtitle">Wop diwop didlidlida !</p>
                                        <p class="name">Verlaine Casgrain - Illuminatrice d'idée</p>
                                </article>
                        </div>
                </article>
        </div>
</section>